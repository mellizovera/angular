import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegistroHomeComponent } from './components/registro-home/registro-home.component';
import { HomeAdentroComponent } from './components/home-adentro/home-adentro.component';
import { EstudiantesComponent } from './components/estudiantes/estudiantes.component';
import { RepresentantesComponent } from './components/representantes/representantes.component';
import { SeguimientoAcademicoComponent } from './components/seguimiento-academico/seguimiento-academico.component';
import { SeguimientoMedicoComponent } from './components/seguimiento-medico/seguimiento-medico.component';
import { TratamientoMedicoComponent } from './components/tratamiento-medico/tratamiento-medico.component';
import { TratamientoCumplidoComponent } from './components/tratamiento-cumplido/tratamiento-cumplido.component';
import { TratamientoNocumplidoComponent } from './components/tratamiento-nocumplido/tratamiento-nocumplido.component';
import { MateriaComponent } from './components/materia/materia.component';
import { MateriaAprobadaComponent } from './components/materia-aprobada/materia-aprobada.component';
import { MateriaReprobadaComponent } from './components/materia-reprobada/materia-reprobada.component';
import { HistorialAcademicoComponent } from './components/historial-academico/historial-academico.component';
import { HistorialMedicoComponent } from './components/historial-medico/historial-medico.component';

const routes: Routes = [


{ path: '',
component: HomeComponent
},
{ path: 'crear-tutor',
component: RegistroHomeComponent
},
{ path: 'adentro',
component: HomeAdentroComponent
},
{ path: 'estudiantes',
component: EstudiantesComponent
},
{ path: 'representantes',
component: RepresentantesComponent
},
{ path: 'seguimiento-medico',
component: SeguimientoMedicoComponent
},
{ path: 'historial-medico',
component: HistorialMedicoComponent
},
{ path: 'tratamiento-medico',
component: TratamientoMedicoComponent
},
{ path: 'tratamiento-cumplido',
component: TratamientoCumplidoComponent
},
{ path: 'tratamiento-nocumplido',
component: TratamientoNocumplidoComponent
},
{ path: 'seguimiento-academico',
component: SeguimientoAcademicoComponent
},
{ path: 'historial-academico',
component: HistorialAcademicoComponent
},
{ path: 'materias',
component: MateriaComponent
},
{ path: 'materias-aprobadas',
component: MateriaAprobadaComponent
},
{ path: 'materias-reprobadas',
component: MateriaReprobadaComponent
},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
