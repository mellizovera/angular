import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-representantes',
  templateUrl: './representantes.component.html',
  styleUrls: ['./representantes.component.css']
})
export class RepresentantesComponent implements OnInit{

 representantes = [
  {
    representado:"Eduardo Ernesto",
    nombres:"Erika ",
    apellidos:"Quimiz",
    cedula:"1256956781",
    ocupacion:"Secretaria"
  },
  {
    representado:"Ana Luisa",
    nombres:"Victoria ",
    apellidos:"Cedeño",
    cedula:"1256956782",
    ocupacion:"Ama de casa"
  },
  {
    representado:"Benito Alfonso",
    nombres:"Linda",
    apellidos:"Alvarado",
    cedula:"1256956783",
    ocupacion:"Veterinaria"
  }
 ]

 estudiantes = [
  {
    nombres:"Eduardo Ernesto"
  },
  {
    nombres:"Antonio Lionel"
  },
  {
    nombres:"Benito Alfonso"
  },
  {
    nombres:"Maria Jose"
  },
  {
    nombres:"Ana Luisa"
  }
]
  
ngOnInit(): void {
  
}
}
