import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-tratamiento-nocumplido',
  templateUrl: './tratamiento-nocumplido.component.html',
  styleUrls: ['./tratamiento-nocumplido.component.css']
})
export class TratamientoNocumplidoComponent  implements OnInit{


  nocumplidos = [
    {
      no:"Antonio Lionel: terapia en Cognitivo-conductual y rehabilitación cognitiva, Entrenamiento en habilidades sociales",
      observacion:"No se siguio el tratamiento mas"
    },
    {
      no:"Eduardo Ernesto: Grupo de apoyo y Amplificadores de sonido portátiles",
      observacion:"No se continuo con el tratamiento"
    }
  ]
constructor(){}
  ngOnInit(): void {
    
  }
}
