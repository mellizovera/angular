import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-materia',
  templateUrl: './materia.component.html',
  styleUrls: ['./materia.component.css']
})
export class MateriaComponent  implements OnInit {

   

  materias = [
    {
      estudiante:"Eduardo Ernesto",
      nombre:"Sistemas distribuidos",
      facultad:"Ciencias de la vida y tecnologia",
      carrera:"Tecnologias de la informacion",
      nivel:"8",
      paralelo:"A",
      jornada:"Matutina"
    },
    {
      estudiante:"Ana Luisa",
      nombre:"Estadistica ||",
      facultad:"Contabilidad y auditoria",
      carrera:"Contabilidad y auditoria",
      nivel:"4",
      paralelo:"A",
      jornada:"Matutina"
    },
    {
      estudiante:"Antonio Lionel",
      nombre:"Calculo integral",
      facultad:"Ingenieria",
      carrera:"Ingenieria civil",
      nivel:"2",
      paralelo:"A",
      jornada:"Matutina"
    }
  ]


  constructor(){}
  ngOnInit(): void {
  
}
}
