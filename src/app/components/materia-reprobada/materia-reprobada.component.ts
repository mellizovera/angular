import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-materia-reprobada',
  templateUrl: './materia-reprobada.component.html',
  styleUrls: ['./materia-reprobada.component.css']
})
export class MateriaReprobadaComponent implements OnInit {

reprobadas = [
  {
    materia:"Eduardo Ernesto: Sistemas distribuidos",
    motivo:"Reprobado por notas"
  }
]

  constructor(){}
  ngOnInit(): void {
  
}
}
