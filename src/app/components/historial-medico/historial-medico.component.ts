import { Component , OnInit} from '@angular/core';

@Component({
  selector: 'app-historial-medico',
  templateUrl: './historial-medico.component.html',
  styleUrls: ['./historial-medico.component.css']
})
export class HistorialMedicoComponent implements OnInit {

  historial = [
    {
      estudiante:"Ana Luisa",
      cedula:"1250652449",
      correoins:"e1250652449@live.uleam.edu.ec",
      discapacidad:"Sensorial",
      motivo:"El representante del paciente realizo la consulta debido a los problemas de percepcion que presenta su representado",
      tratamientop:"Comunicacion entre personas, propiciando las relaciones sociales",
      tratamientof:"Estimulacion de todos los sentidos de la joven",
      fechai:"2023-05-19",
      fechaf:"2023-09-19"
    },
    {
      estudiante:"Ana Luisa",
      cedula:"1250652449",
      correoins:"e1250652449@live.uleam.edu.ec",
      discapacidad:"Sensorial",
      motivo:"El representante del paciente realizo la consulta debido a los problemas de percepcion que presenta su representado",
      tratamientop:"Comunicacion entre personas, propiciando las relaciones sociales",
      tratamientof:"Estimulacion de todos los sentidos de la joven",
      fechai:"2023-05-19",
      fechaf:"2023-09-19"
    },
    {
      estudiante:"Ana Luisa",
      cedula:"1250652449",
      correoins:"e1250652449@live.uleam.edu.ec",
      discapacidad:"Sensorial",
      motivo:"El representante del paciente realizo la consulta debido a los problemas de percepcion que presenta su representado",
      tratamientop:"Comunicacion entre personas, propiciando las relaciones sociales",
      tratamientof:"Estimulacion de todos los sentidos de la joven",
      fechai:"2023-05-19",
      fechaf:"2023-09-19"
    },
    {
      estudiante:"Ana Luisa",
      cedula:"1250652449",
      correoins:"e1250652449@live.uleam.edu.ec",
      discapacidad:"Sensorial",
      motivo:"El representante del paciente realizo la consulta debido a los problemas de percepcion que presenta su representado",
      tratamientop:"Comunicacion entre personas, propiciando las relaciones sociales",
      tratamientof:"Estimulacion de todos los sentidos de la joven",
      fechai:"2023-05-19",
      fechaf:"2023-09-19"
    },
  ]


  constructor(){}
  ngOnInit(): void {
  
}
}
