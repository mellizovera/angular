import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-materia-aprobada',
  templateUrl: './materia-aprobada.component.html',
  styleUrls: ['./materia-aprobada.component.css']
})
export class MateriaAprobadaComponent implements OnInit {

 aprobadas = [
  {
   materia:"Antonio Lionel: Calculo integral",
   promedio:"15.60",
   observacion:"Paso con un buen promedio"
  },
  {
    materia:"Ana Luisa: Estadistica ||",
    promedio:"19.80",
    observacion:"Muy buena estudiante"
   }
 ]


  constructor(){}
  ngOnInit(): void {
  
}
}
