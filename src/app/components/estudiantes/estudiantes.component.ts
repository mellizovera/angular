import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.css']
})
export class EstudiantesComponent implements OnInit {

   estudiantes = [
    {
      nombres:"Eduardo Ernesto",
      apellidos:"Gonzales Zambrano",
      cedula:"1250652445",
      edad:"22",
      email:"e1250652445@live.uleam.edu.ec",
      nacimiento:"2001-04-28"
    },
    {
      nombres:"Antonio Lionel",
      apellidos:"Cevallos Rodriguez",
      cedula:"1250652446",
      edad:"22",
      email:"e1250652446@live.uleam.edu.ec",
      nacimiento:"2001-04-29"
    },
    {
      nombres:"Benito Alfonso",
      apellidos:"Rivas Flores",
      cedula:"1250652447",
      edad:"22",
      email:"e1250652447@live.uleam.edu.ec",
      nacimiento:"2001-04-30"
    },
    {
      nombres:"Maria Jose",
      apellidos:"Santana Quiroz",
      cedula:"1250652448",
      edad:"22",
      email:"e1250652448@live.uleam.edu.ec",
      nacimiento:"2001-05-01"
    },
    {
      nombres:"Ana Luisa",
      apellidos:"Parraga Almeida",
      cedula:"1250652449",
      edad:"22",
      email:"e1250652449@live.uleam.edu.ec",
      nacimiento:"2001-05-02"
    }
  ]

  constructor(){}
ngOnInit(): void {
  
}

}
