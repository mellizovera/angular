import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tratamiento-cumplido',
  templateUrl: './tratamiento-cumplido.component.html',
  styleUrls: ['./tratamiento-cumplido.component.css']
})
export class TratamientoCumplidoComponent implements OnInit {

cumplidos = [
  {
      cumplido:"Ana Luisa: Comunicacion entre personas, propiciando las relaciones sociales , Estimulacion de todos los sentidos del joven",
      observacion:"Se llevo a cabo al pie de la letra el tratamiento",
      inicio:"2023-05-19",
      fin:"2023-09-19"
  }
]

  constructor(){}
ngOnInit(): void {
  
}
}
