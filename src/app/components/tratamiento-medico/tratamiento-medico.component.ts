import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tratamiento-medico',
  templateUrl: './tratamiento-medico.component.html',
  styleUrls: ['./tratamiento-medico.component.css']
})
export class TratamientoMedicoComponent implements OnInit {



  tratamientos = [
    {
      discapacidad:"Auditiva",
      estudiante:"Eduardo Ernesto",
      psicologico:"Grupo de apoyo",
      fisico:"Amplificadores de sonido portátiles",
      descripcion:"El representante del paciente realizo la consulta para intentar la implantacion de algun tipo de audifono",
      opinion:"El paciente presenta problemas todos los dias de audicion",
      fecha:"2023-05-16"
    },
    {
      discapacidad:"Psicosocial",
      estudiante:"Antonio Lionel",
      psicologico:"terapia en Cognitivo-conductual y rehabilitación cognitiva",
      fisico:"Entrenamiento en habilidades sociales",
      descripcion:"El representante del paciente fue a la consulta esperando respuesta para una posible terapia",
      opinion:"El paciente se encuentra siempre aislado de la sociedad",
      fecha:"2023-05-17"
    },
    {
      discapacidad:"Sensorial",
      estudiante:"Ana Luisa",
      psicologico:"Comunicacion entre personas, propiciando las relaciones sociales",
      fisico:"Estimulacion de todos los sentidos de la joven",
      descripcion:"El representante del paciente realizo la consulta debido a los problemas de percepcion que presenta su representado",
      opinion:"El paciente tiene problemas para percibir el tacto",
      fecha:"2023-05-18"
    },
  ]

  constructor(){}
ngOnInit(): void {
  
}
}
